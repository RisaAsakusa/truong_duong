//Code pagetop scroll
$(window).scroll(function(){
	if ($(window).scrollTop()>100) {
		$(".page-scroll").show();
	}else{
		$(".page-scroll").hide();
	}
});

function page_scrolltop(){
	$("html, body").animate({
		scrollTop: 0
	}, "800");
}


// Code slideshow

/*Slide show*/

$(document).ready(function(){
  $(".owl-carousel").owlCarousel({
  	items: 1,
  	loop: true,
  	dots: true,
  	nav: true,
  	navText: ['<img class="arrow arrowleft" src="assets/img/images/mobile/slider_arrow_left_phone.png" />','<img class="arrow arrowright" src="assets/img/images/mobile/slider_arrow_right_phone.png" />'],
  });
});




