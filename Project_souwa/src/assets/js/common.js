/*Code slide animate*/

$(".banner-img:gt(0)").hide();
setInterval(function() {
  $('.banner-img:first')
    .fadeOut(1000)
    .next()
    .fadeIn(1000)
    .end()
    .appendTo('.index-banner');
}, 3000);


/*Code scroll pagetop*/

$(window).scroll(function(){
	if ($(window).scrollTop()>100) {
		$(".page-top-img").show();
	}else{
		$(".page-top-img").hide();
	}
});

function page_scrolltop(){
	$("html, body").animate({
		scrollTop: 0
	}, "800");
}


/*Code lightbox*/




/*Code case active button case.html*/

$(".case-btn1 > a").click(function(){
	if ($(".case-btn1-img1").hasClass("active")) {
		$(".case-btn1-img1").removeClass("active");
		$(".case-btn1-img1").hide();
		$(".case-btn1-img2").addClass("active");
		$(".case-btn1-img2").show();
	}else{
		$(".case-btn1-img2").removeClass("active");
		$(".case-btn1-img2").hide();
		$(".case-btn1-img1").addClass("active");
		$(".case-btn1-img1").show();
	}
});

/*Code activemenu*/

/*$(".nav-link").click(function(){
    $(".nav-link").removeClass("activemenu");
    $(this).addClass("activemenu");
})*/

/*Code btn contact.html*/

$(".confirm-btn").click(function(){
	$(".back-btn").show();
	$(".send-btn").show();	
	$(".confirm-btn").hide();
});

$(".back-btn").click(function(){
	$(".confirm-btn").show();
	$(".back-btn").hide();
	$(".send-btn").hide();
})





